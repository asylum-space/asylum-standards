# MINTITEM

Mint [item](../entities/item.md) from [blueprint](../entities/blueprint.md). Item will have the same set of [interpretations](../entities/interpretation.md) as blueprint has.

## ARGUMENTS

Pass appropriate arguments to corresponding extrinsic of the asylum-blueprints pallet directly or via connection lib. Let's describe arguments as JSON scheme:

```json
{
  "owner": {
    "type": "string",
    "description": "The owner of minted item"
  },
  "blueprint-id": {
    "type": "number",
    "description": "The id of the blueprint"
  },
  "metadata": {
    "type": "string",
    "description": "HTTP(s) or IPFS URI. If IPFS, MUST be in the format of ipfs://hash"
  }
}  
```

## EXAMPLE

```json
{
  "owner": "CpjsLDC1JFyrhm3ftC9Gs4QoyrkHKhZKtK7YqGTRFtTafgp",
  "blueprint-id": 42,
  "metadata": "ipfs://QmavoTVbVHnGEUztnBT2p3rif3qBPeCfyyUE5v4Z7oFvs4"
}  
```