# BURNITEM

Burn [item](../entities/item.md)

## ARGUMENTS

Pass appropriate arguments to corresponding extrinsic of the asylum-blueprints pallet directly or via connection lib. Let's describe arguments as JSON scheme:

```json
{
  "blueprint-id": {
    "type": "number",
    "description": "The id of the blueprint"
  },
  "item-id": {
    "type": "number",
    "description": "The id of the item to be burned"
  }
}
```

## EXAMPLE

```json
{
  "blueprint-id": 42,
  "item-id": 453
}